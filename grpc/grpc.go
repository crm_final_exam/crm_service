package grpc

import (
	"crm_service/config"
	"crm_service/genproto/crm_service"
	"crm_service/grpc/client"
	"crm_service/grpc/service"
	"crm_service/pkg/logger"
	"crm_service/storage"

	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
)

func SetUpServer(cfg config.Config, log logger.LoggerI, strg storage.StorageI, srvc client.ServiceManagerI) (grpcServer *grpc.Server) {

	grpcServer = grpc.NewServer()

	crm_service.RegisterBranchServiceServer(grpcServer, service.NewBranchService(cfg, log, strg, srvc))
	crm_service.RegisterSuperAdminServiceServer(grpcServer, service.NewSuperAdminService(cfg, log, strg, srvc))
	crm_service.RegisterManagerServiceServer(grpcServer, service.NewManagerService(cfg, log, strg, srvc))
	crm_service.RegisterTeacherServiceServer(grpcServer, service.NewTeacherService(cfg, log, strg, srvc))
	crm_service.RegisterSupportTeacherServiceServer(grpcServer, service.NewSupportTeacherService(cfg, log, strg, srvc))
	crm_service.RegisterAdministratorServiceServer(grpcServer, service.NewAdministratorService(cfg, log, strg, srvc))
	crm_service.RegisterStudentServiceServer(grpcServer, service.NewStudentService(cfg, log, strg, srvc))
	crm_service.RegisterGroupServiceServer(grpcServer, service.NewGroupService(cfg, log, strg, srvc))
	crm_service.RegisterEventServiceServer(grpcServer, service.NewEventService(cfg, log, strg, srvc))
	crm_service.RegisterAssignStudentServiceServer(grpcServer, service.NewAssignStudentService(cfg, log, strg, srvc))
	crm_service.RegisterDoTaskServiceServer(grpcServer, service.NewDoTaskService(cfg, log, strg, srvc))
	crm_service.RegisterScoreServiceServer(grpcServer, service.NewScoreService(cfg, log, strg, srvc))
	crm_service.RegisterJurnalServiceServer(grpcServer, service.NewJurnalService(cfg, log, strg, srvc))
	crm_service.RegisterTaskServiceServer(grpcServer, service.NewTaskService(cfg, log, strg, srvc))
	crm_service.RegisterScheduleServiceServer(grpcServer, service.NewScheduleService(cfg, log, strg, srvc))
	crm_service.RegisterLessonServiceServer(grpcServer, service.NewLessonService(cfg, log, strg, srvc))
	crm_service.RegisterPaymentServiceServer(grpcServer, service.NewPaymentService(cfg, log, strg, srvc))

	reflection.Register(grpcServer)
	return
}
