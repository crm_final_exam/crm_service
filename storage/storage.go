package storage

import (
	"context"
	"crm_service/genproto/crm_service"
)

type StorageI interface {
	CloseDB()
	SuperAdmin() SuperAdminRepoI
	Branch() BranchRepoI
	Manager() ManagerRepoI
	Teacher() TeacherRepoI
	SupportTeacher() SupportTeacherRepoI
	Administrator() AdministratorRepoI
	Group() GroupRepoI
	Student() StudentRepoI
	Event() EventRepoI
	AssignStudent() AssignStudentRepoI
	DoTask() DoTaskRepoI
	Score() ScoreRepoI
	Jurnal() JurnalRepoI
	Task() TaskRepoI
	Schedule() ScheduleRepoI
	Lesson() LessonRepoI
	Payment() PaymentRepoI
}

type SuperAdminRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateSuperAdmin) (resp *crm_service.SuperAdminPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.SuperAdminPrimaryKey) (resp *crm_service.SuperAdmin, err error)
	GetAll(ctx context.Context, req *crm_service.GetListSuperAdminRequest) (resp *crm_service.GetListSuperAdminResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateSuperAdmin) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.SuperAdminPrimaryKey) error
}

type BranchRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateBranch) (resp *crm_service.BranchPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.BranchPrimaryKey) (resp *crm_service.Branch, err error)
	GetAll(ctx context.Context, req *crm_service.GetListBranchRequest) (resp *crm_service.GetListBranchResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateBranch) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.BranchPrimaryKey) error
}

type ManagerRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateManager) (resp *crm_service.ManagerPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.ManagerPrimaryKey) (resp *crm_service.Manager, err error)
	GetAll(ctx context.Context, req *crm_service.GetListManagerRequest) (resp *crm_service.GetListManagerResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateManager) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.ManagerPrimaryKey) error
}

type TeacherRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateTeacher) (resp *crm_service.TeacherPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.TeacherPrimaryKey) (resp *crm_service.Teacher, err error)
	GetAll(ctx context.Context, req *crm_service.GetListTeacherRequest) (resp *crm_service.GetListTeacherResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateTeacher) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.TeacherPrimaryKey) error
	// TeacherPanel(ctx context.Context, req *crm_service.TeacherPanelRequest) (resp *crm_service.TeacherPanelResponse, err error)
}

type SupportTeacherRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateSupportTeacher) (resp *crm_service.SupportTeacherPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.SupportTeacherPrimaryKey) (resp *crm_service.SupportTeacher, err error)
	GetAll(ctx context.Context, req *crm_service.GetListSupportTeacherRequest) (resp *crm_service.GetListSupportTeacherResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateSupportTeacher) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.SupportTeacherPrimaryKey) error
}

type AdministratorRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateAdministrator) (resp *crm_service.AdministratorPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.AdministratorPrimaryKey) (resp *crm_service.Administrator, err error)
	GetAll(ctx context.Context, req *crm_service.GetListAdministratorRequest) (resp *crm_service.GetListAdministratorResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateAdministrator) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.AdministratorPrimaryKey) error
}
type GroupRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateGroup) (resp *crm_service.GroupPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.GroupPrimaryKey) (resp *crm_service.Group, err error)
	GetAll(ctx context.Context, req *crm_service.GetListGroupRequest) (resp *crm_service.GetListGroupResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateGroup) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.GroupPrimaryKey) error
}

type StudentRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateStudent) (resp *crm_service.StudentPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.StudentPrimaryKey) (resp *crm_service.Student, err error)
	GetAll(ctx context.Context, req *crm_service.GetListStudentRequest) (resp *crm_service.GetListStudentResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateStudent) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.StudentPrimaryKey) error
	GetStudetReport(ctx context.Context, req *crm_service.StudentReportRequest) (resp *crm_service.StudentReportResponse, err error)
}

type DoTaskRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateDoTask) (resp *crm_service.DoTaskPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.DoTaskPrimaryKey) (resp *crm_service.DoTask, err error)
	GetAll(ctx context.Context, req *crm_service.GetListDoTaskRequest) (resp *crm_service.GetListDoTaskResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateDoTask) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.DoTaskPrimaryKey) error
}

type EventRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateEvent) (resp *crm_service.EventPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.EventPrimaryKey) (resp *crm_service.Event, err error)
	GetAll(ctx context.Context, req *crm_service.GetListEventRequest) (resp *crm_service.GetListEventResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateEvent) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.EventPrimaryKey) error
}

type ScoreRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateScore) (resp *crm_service.ScorePrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.ScorePrimaryKey) (resp *crm_service.Score, err error)
	GetAll(ctx context.Context, req *crm_service.GetListScoreRequest) (resp *crm_service.GetListScoreResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateScore) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.ScorePrimaryKey) error
}

type AssignStudentRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateAssignStudent) (resp *crm_service.AssignStudentPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.AssignStudentPrimaryKey) (resp *crm_service.AssignStudent, err error)
	GetAll(ctx context.Context, req *crm_service.GetListAssignStudentRequest) (resp *crm_service.GetListAssignStudentResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateAssignStudent) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.AssignStudentPrimaryKey) error
}

type JurnalRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateJurnal) (resp *crm_service.JurnalPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.JurnalPrimaryKey) (resp *crm_service.Jurnal, err error)
	GetAll(ctx context.Context, req *crm_service.GetListJurnalRequest) (resp *crm_service.GetListJurnalResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateJurnal) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.JurnalPrimaryKey) error
}
type TaskRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateTask) (resp *crm_service.TaskPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.TaskPrimaryKey) (resp *crm_service.Task, err error)
	GetAll(ctx context.Context, req *crm_service.GetListTaskRequest) (resp *crm_service.GetListTaskResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateTask) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.TaskPrimaryKey) error
}

type ScheduleRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateSchedule) (resp *crm_service.SchedulePrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.SchedulePrimaryKey) (resp *crm_service.Schedule, err error)
	GetAll(ctx context.Context, req *crm_service.GetListScheduleRequest) (resp *crm_service.GetListScheduleResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateSchedule) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.SchedulePrimaryKey) error
	GetScheduleReport(ctx context.Context, req *crm_service.ScheduleReportRequest) (resp *crm_service.ScheduleReportResponse, err error)
	GetScheduleMonthReport(ctx context.Context, req *crm_service.ScheduleReportRequest) (resp *crm_service.ScheduleReportResponse, err error)
}

type LessonRepoI interface {
	Create(ctx context.Context, req *crm_service.CreateLesson) (resp *crm_service.LessonPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.LessonPrimaryKey) (resp *crm_service.Lesson, err error)
	GetAll(ctx context.Context, req *crm_service.GetListLessonRequest) (resp *crm_service.GetListLessonResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdateLesson) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.LessonPrimaryKey) error
}

type PaymentRepoI interface {
	Create(ctx context.Context, req *crm_service.CreatePayment) (resp *crm_service.PaymentPrimaryKey, err error)
	GetByPKey(ctx context.Context, req *crm_service.PaymentPrimaryKey) (resp *crm_service.Payment, err error)
	GetAll(ctx context.Context, req *crm_service.GetListPaymentRequest) (resp *crm_service.GetListPaymentResponse, err error)
	Update(ctx context.Context, req *crm_service.UpdatePayment) (rowsAffected int64, err error)
	Delete(ctx context.Context, req *crm_service.PaymentPrimaryKey) error
}
